import * as fs from 'fs';
import { browser, by, element } from 'protractor';
import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message, submit button disabled', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to angular-quality!');
    expect(page.isSubmitButtonDisabled()).toBe('true');
  });

  it('should enable welcome button, if data is entered', () => {
    page.navigateTo();
    page.fillNameInput('Helene Birne');
    page.fillCustomerNumberInput(125);
    expect(page.isSubmitButtonDisabled()).toBeNull();
  });

  it('should show alert if customerNumber is bigger 10000', () => {
    page.navigateTo();
    page.fillCustomerNumberInput(300000);
    expect(page.getPremiumAlert().getText()).toBe('Premiumkunde!');
  });

  it('should show Premium-Angebote if Premium-Customer', () => {
    page.navigateTo();
    page.fillNameInput('Helene Birne');
    page.fillCustomerNumberInput(300000);
    page.getSubmitButton().click();
    browser.takeScreenshot()
      .then(data => {
        const stream = fs.createWriteStream('./e2e/screenshots/demo.png');
          stream.write(new Buffer(data, 'base64'));
          stream.end();
      });
    expect(page.getPremimOffers().getText()).toContain('Premiumangebote');
  });

  it('should show only standard offers if normal customer', () => {
    page.navigateTo();
    page.fillNameInput('Martha Pfahl');
    page.fillCustomerNumberInput(3000);
    browser.actions().mouseMove(page.getSubmitButton()).perform();
    browser.actions().click();
    expect(page.getPremimOffers().isPresent()).toBeFalsy();
  });
});
