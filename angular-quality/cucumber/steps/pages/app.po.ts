import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    browser.ignoreSynchronization = true;
    return browser.get('/').then(() => {
      browser.ignoreSynchronization = false; });
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }

  fillNameInput(value: string) {
    return element(by.id('customerName')).sendKeys(value);
  }

  fillCustomerNumberInput(value: number) {
    return element(by.id('customerId')).sendKeys(value);
  }

  getPremiumAlert() {
    return element(by.id('specialCustomer'));
  }

  getSubmitButton() {
    return element(by.css('section form .btn.btn-primary'));
  }

  isSubmitButtonDisabled() {
    return this.getSubmitButton().getAttribute('disabled');
  }

  getPremimOffers() {
    return element(by.css('.row h2.col-12'));
  }
}
