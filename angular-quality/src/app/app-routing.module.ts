import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Route, RouterModule } from '@angular/router';
import { InputComponent } from './input/input.component';
import { OffersComponent } from './offers/offers.component';

const routes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/input'
  },
  {
    path: 'input',
    component: InputComponent
  },
  {
    path: 'offers',
    component: OffersComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
