import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';

import { catchError, concatMap, map } from 'rxjs/operators';
import { OrderService } from '../orders/order.service';
import { OrderActions, OrderActionTypes, UpdateOrder, UpdateOrderFailed, UpdateOrderSuccess } from './order.actions';


@Injectable()
export class OrderEffects {


  @Effect()
  updateOrder$ = this.actions$.pipe(
    ofType<UpdateOrder>(OrderActionTypes.Update),
    concatMap(action => this.service.updateOrder(action.payload.item)
      .pipe(
        map(order => new UpdateOrderSuccess(order)),
        catchError(err => {
          console.log(err);
          return of(new UpdateOrderFailed());
        })
      )
    )
  );


  constructor(private service: OrderService, private actions$: Actions<OrderActions>) {
  }

}
