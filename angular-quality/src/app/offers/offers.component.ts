import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { hasCustomer, isCustomerPremium, State } from '../store';
import { UpdateOrder } from '../store/order.actions';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html'
})
export class OffersComponent implements OnInit {

  isCustomerPremium: Observable<boolean>;
  hasCustomer: Observable<boolean>;

  constructor(private store: Store<State>, private router: Router) {
  }

  ngOnInit() {
    this.hasCustomer = this.store.pipe(
      select(hasCustomer),
      tap(customer => {
        if (!customer) {
          this.router.navigateByUrl('/');
        }
      })
    );
    this.isCustomerPremium = this.store.pipe(select(isCustomerPremium));
  }

  order(item: string): void {
    this.store.dispatch(new UpdateOrder({item}));
  }


}
