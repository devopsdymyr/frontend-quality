import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  updateOrder(item: string): Observable<{items: string[]}> {
    return this.http.post<{items: string[]}>('http://localhost:8080/api/order', {item});
  }
}
