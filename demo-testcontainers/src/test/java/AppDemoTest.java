import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class AppDemoTest {

    private AppDemo appDemo;

    @org.junit.Before
    public void setUp() throws Exception {
        appDemo = new AppDemo();
    }

    @org.junit.After
    public void tearDown() throws Exception {
        appDemo.tearDownLogic();
    }

    @org.junit.Test
    public void add() {
        assertThat(appDemo.add(3,5),is(8));
    }
}